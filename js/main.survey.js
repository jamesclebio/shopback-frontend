;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.survey = {
    settings: {
      autoinit: true,
      home: '.Pages--home',
      answer: '.Answer',
      products: '.Answer--carrossel .products',
      nav: '.Home--routes',
      back: '.btn--back',
      endpoint: 'endpoints/products/products.json'
    },

    init: function () {
      var $products = $(this.settings.products);

      this.builder($products);
      this.handler($products);
    },

    builder: function ($products) {
      this.products($products);
    },

    handler: function ($products) {
      var that = this;
      var $home = $(this.settings.home);
      var $answer = $(this.settings.answer);

      // Nav
      $(this.settings.nav).find('li').on({
        click: function () {
          that.nav($home, $answer, $products, $(this).index());
        }
      });

      // Back
      $(this.settings.back).on({
        click: function (event) {
          that.nav($home, $answer, $products);
          event.preventDefault();
        }
      });
    },

    nav: function ($home, $answer, $products, index) {
      if (index || index === 0) {
        $home.hide();
        $($answer[index]).fadeIn(100);

        if (index === 2) {
          $products.slick('reinit');
        }
      } else {
        $answer.hide();
        $home.fadeIn(100);
      }
    },

    products: function ($products) {
      var that = this;

      $.ajax({
        url: that.settings.endpoint,

        beforeSend: function () {
          $products.empty();
        },

        error: function () {
          alert('Ops! Algo deu errado... :(');
        },

        success: function (data) {
          var html = '';

          for (var i in data) {
            html += '<a href="' + data[i].link + '" class="item">';
            html += '  <img src="' + data[i].imagem + '" alt="">';
            html += '  <div class="name">' + data[i].nome + '</div>';
            html += '  <div class="price">' + data[i].preco + '</div>';
            html += '</a>';
          }

          $products.html(html);
          that.carousel($products);
        }
      });
    },

    carousel: function ($products) {
      $products.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000
      });
    }
  };
}(jQuery, this, this.document));
