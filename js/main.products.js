;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.products = {
    settings: {
      autoinit: true,
      main: '.Recomendacao--products .products',
      thanks: '.Overlay.Recomendacao--thanks',
      thanksTrigger: '.btn--facebook',
      endpoints: {
        products: 'endpoints/products/products.json',
        facebook: 'endpoints/facebook/facebook.json'
      }
    },

    init: function () {
      var $main = $(this.settings.main);
      var $thanks = $(this.settings.thanks);

      this.builder($main, $thanks);
      this.handler($thanks);
    },

    builder: function ($main, $thanks) {
      this.items($main);
      this.thanks($thanks);
    },

    handler: function ($thanks) {
      $(this.settings.thanksTrigger).on({
        click: function (event) {
          $thanks.fadeToggle(100);
          event.preventDefault();
        }
      });
    },

    items: function ($main) {
      var that = this;

      $.ajax({
        url: that.settings.endpoints.products,

        beforeSend: function () {
          $main.empty();
        },

        error: function () {
          alert('Ops! Algo deu errado... :(');
        },

        success: function (data) {
          var html = '';

          for (var i in data) {
            html += '<a href="' + data[i].link + '" class="item">';
            html += '  <img src="' + data[i].imagem + '" alt="">';
            html += '  <div class="name">' + data[i].nome + '</div>';
            html += '  <div class="price">' + data[i].preco + '</div>';
            html += '</a>';
          }

          $main.html(html);
          that.carousel($main);
        }
      });
    },

    carousel: function ($main) {
      $main.slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay: true,
        autoplaySpeed: 3000
      });
    },

    thanks: function ($thanks) {
      var that = this;

      $.ajax({
        url: that.settings.endpoints.facebook,

        error: function () {
          alert('Ops! Algo deu errado... :(');
        },

        success: function (data) {
          $thanks.find('img').attr({
              'src': data.imagem,
              'alt': data.nome
          });
          $thanks.find('h4').text(data.nome);
          $thanks.find('h5').text(data.email);
        }
      });
    }
  };
}(jQuery, this, this.document));
