var compass = require('gulp-compass');
var concat = require('gulp-concat');
var connect = require('gulp-connect');
var del = require('del');
var gulp = require('gulp');
var inlineCss = require('gulp-inline-css');
var jshint = require('gulp-jshint');
var plumber = require('gulp-plumber');
var runSequence = require('run-sequence');
var styleInject = require("gulp-style-inject");
var taskListing = require('gulp-task-listing');
var uglify = require('gulp-uglify');

// Default
gulp.task('default', taskListing);

// Dev
gulp.task('dev', ['connect', 'watch']);

// Connect
gulp.task('connect', function () {
  connect.server({
    port: 8000,
    livereload: true
  });
});

// Watch
gulp.task('watch', function () {
  gulp.watch('sass/**/*', ['styles']);
  gulp.watch('js/**/*', ['scripts']);
  gulp.watch('*.html', ['html']);
});

// Styles
gulp.task('styles', function () {
  return gulp.src('sass/**/*')
    .pipe(plumber())
    .pipe(compass({
      style: 'expanded',
      sass: 'sass',
      css: 'assets/css',
      image: 'assets/img'
    }))
    .pipe(connect.reload());
});

// Scripts
gulp.task('scripts', function () {
  return del('assets/js/main.js', function () {
    gulp
      .src([
        'js/main.*.js',
        'js/main.js'
      ])
      .pipe(plumber())
      .pipe(jshint())
      .pipe(jshint.reporter('default'))
      .pipe(concat('main.js'))
      .pipe(uglify())
      .pipe(gulp.dest('assets/js'))
      .pipe(connect.reload());
  });
});

// HTML
gulp.task('html', function () {
  return gulp.src('*.html')
    .pipe(connect.reload());
});

// Build
gulp.task('build', function () {
  runSequence('build:clean', 'build:base', ['build:html-1', 'build:html-2-3']);
});

gulp.task('build:clean', function () {
  return del.sync('dist');
});

gulp.task('build:base', function () {
  return gulp.src([
    'assets/**/*',
    'endpoints/**/*'
    ], {'base': '.'})
    .pipe(gulp.dest('dist/'));
});

gulp.task('build:html-1', function () {
  return gulp.src('Tarefa--1.html')
    .pipe(styleInject())
    .pipe(inlineCss({
      preserveMediaQueries: true,
      removeStyleTags: true,
      applyWidthAttributes: true,
      applyTableAttributes: true,
      removeHtmlSelectors: true
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('build:html-2-3', function () {
  return gulp.src([
    'Tarefa--2.html',
    'Tarefa--3.html'
    ])
    .pipe(gulp.dest('dist'));
});
