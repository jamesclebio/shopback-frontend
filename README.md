#ShopBack Front-End

## Requisitos para o ambiente de desenvolvimento

- [Node.js](http://nodejs.org/)
- [npm](https://www.npmjs.org/)
- [gulp.js](http://gulpjs.com/)
- [Bower](http://bower.io/)
- [Compass](http://compass-style.org/)

## Comandos gulp

`gulp dev` Sobe um servidor em *http://localhost:8000*

`gulp styles` compila os arquivos de */sass*

`gulp scripts` gera o */assets/js/main.js*

`gulp build` faz o build do projeto em */dist* (arquivos para produção)

## Notas

Como o projeto possui requisições AJAX, deverá ser testado rodando em um servidor...

O comando `gulp dev`, além de subir o servidor local, também assiste os arquivos source do projeto, executando automaticamente as respectivas tasks (styles, scripts) sempre que um arquivo é alterado, dando reload automático no navegador (livereload).

Nos arquivos JavaScript source (*/js*) foi utilizado o padrão de design *Module Pattern*. Cada arquivo possui os módulos de seu respectivo escopo de funcionalidade.
